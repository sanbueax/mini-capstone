const User = require("../models/user")
const auth = require("../auth")
const { OAuth2Client } = require('google-auth-library')
const clientId = '628236732996-f23vfnvoplq78jo4bjbmcjf22n3lde30.apps.googleusercontent.com'
require('dotenv').config()

//login
module.exports.login = (params) => {
	return User.findOne({email: params.email}).then(resultFromFindOne => {
		if(resultFromFindOne === null){ //user does not exist
			return {error: 'does-not-exist'}
		}

		if (resultFromFindOne.loginType !== 'email') { 
        	return { error: 'login-type-error' }
        }
	})
}

//get user profile
module.exports.get = (params) => {
	return User.findById(params.userId).then(resultFromFindById => {
		resultFromFindById.password = undefined
		return resultFromFindById
	})
}

module.exports.verifyGoogleTokenId = async (tokenId) => {
   const client = new OAuth2Client(clientId);
   const data = await client.verifyIdToken({
       idToken: tokenId,
       audience: clientId,
   });

   if (data.payload.email_verified === true) {
   const user = await User.findOne({ email: data.payload.email }).exec();

       if (user !== null) {
           if (user.loginType === "google") {
               return { accessToken: auth.createAccessToken(user.toObject()) };
           } else {
               return { error: "login-type-error" };
           }
       } else {
           const newUser = new User({
               firstName: data.payload.given_name,
               lastName: data.payload.family_name,
               email: data.payload.email,
               loginType: "google",
           });

           return newUser.save().then((user, err) => {
               return { accessToken: auth.createAccessToken(user.toObject()) };
           });
       }
   } else {
       return { error: "google-auth-error" };
   }
}