//dependencies setup
const express = require("express");
const mongoose = require("mongoose")
const cors = require("cors");
const userRoutes = require("./routes/user");

//database connection
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))
mongoose.connect('mongodb+srv://jayjay1914:sanbueax1914@cluster0.kyzq3.mongodb.net/course_booking2?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

//server setup
const app = express();
const port = 4000;
//bodyparser middleware
app.use(express.json()) //only looks at request where the Content-Type header is JSON
app.use(express.urlencoded({extended: true})) //allows POST requests to include nested objects

//configure cors
// const corsOptions = {
// 	origin: 'http://localhost:3000',
// 	optionsSuccessStatus: 200
// }
app.use(cors()) //this will be used if other sites try to access our API

//add all the routes
// app.use("/api/courses", courseRoutes);
app.use("/api/users", userRoutes);

//server listening
app.listen(port, () => console.log(`Listening to port ${port}`));