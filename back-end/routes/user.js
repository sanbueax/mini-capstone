const express = require("express")
const router = express.Router()
const auth = require("../auth")
const UserController = require("../controllers/user")

//login
router.post('/login', (req, res) => {
	UserController.login(req.body).then(resultFromLogin => res.send(resultFromLogin))
})

//get details of a user
//auth.verify ensures that a user is logged in before proceeding to the next part of the code
router.get('/details', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	//the result is the data from auth.js (id, email, isAdmin)

	//we use the id of the user from the token to search for the user's information
	UserController.get({userId: user.id}).then(user => res.send(user))
})

//verify google log in token
router.post('/verify-google-id-token', async (req, res) => {
	res.send(await UserController.verifyGoogleTokenId(req.body.tokenId))
})

module.exports = router