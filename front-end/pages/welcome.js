import { useState, useEffect } from 'react';
import { Card, Button, Row, Col } from 'react-bootstrap'
import Router from 'next/router'
import AppHelper from '../apphelper'
import View from '../components/View'

export default function welcome(){
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')

    useEffect(() => {
        fetch(`${ AppHelper.API_URL }/users/details`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setFirstName(data.firstName)
            setLastName(data.lastName)
        })
    }, [])

    return (
        <View title={ 'Welcome' }>
            <Row className="justify-content-center">
                <Col xs md="6">
                    <Card>
                        <Card.Header className="text-center">Welcome, {firstName} {lastName}</Card.Header>
                        <Card.Body>
                            <Button className="w-100 text-center d-flex justify-content-center" href="/logout">Logout</Button>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </View>        
    )
}